# BambangShop Publisher App
Tutorial and Example for Advanced Programming 2024 - Faculty of Computer Science, Universitas Indonesia

---

## About this Project
In this repository, we have provided you a REST (REpresentational State Transfer) API project using Rocket web framework.

This project consists of four modules:
1.  `controller`: this module contains handler functions used to receive request and send responses.
    In Model-View-Controller (MVC) pattern, this is the Controller part.
2.  `model`: this module contains structs that serve as data containers.
    In MVC pattern, this is the Model part.
3.  `service`: this module contains structs with business logic methods.
    In MVC pattern, this is also the Model part.
4.  `repository`: this module contains structs that serve as databases and methods to access the databases.
    You can use methods of the struct to get list of objects, or operating an object (create, read, update, delete).

This repository provides a basic functionality that makes BambangShop work: ability to create, read, and delete `Product`s.
This repository already contains a functioning `Product` model, repository, service, and controllers that you can try right away.

As this is an Observer Design Pattern tutorial repository, you need to implement another feature: `Notification`.
This feature will notify creation, promotion, and deletion of a product, to external subscribers that are interested of a certain product type.
The subscribers are another Rocket instances, so the notification will be sent using HTTP POST request to each subscriber's `receive notification` address.

## API Documentations

You can download the Postman Collection JSON here: https://ristek.link/AdvProgWeek7Postman

After you download the Postman Collection, you can try the endpoints inside "BambangShop Publisher" folder.
This Postman collection also contains endpoints that you need to implement later on (the `Notification` feature).

Postman is an installable client that you can use to test web endpoints using HTTP request.
You can also make automated functional testing scripts for REST API projects using this client.
You can install Postman via this website: https://www.postman.com/downloads/

## How to Run in Development Environment
1.  Set up environment variables first by creating `.env` file.
    Here is the example of `.env` file:
    ```bash
    APP_INSTANCE_ROOT_URL="http://localhost:8000"
    ```
    Here are the details of each environment variable:
    | variable              | type   | description                                                |
    |-----------------------|--------|------------------------------------------------------------|
    | APP_INSTANCE_ROOT_URL | string | URL address where this publisher instance can be accessed. |
2.  Use `cargo run` to run this app.
    (You might want to use `cargo check` if you only need to verify your work without running the app.)

## Mandatory Checklists (Publisher)
-   [ ] Clone https://gitlab.com/ichlaffterlalu/bambangshop to a new repository.
-   **STAGE 1: Implement models and repositories**
    -   [ ] Commit: `Create Subscriber model struct.`
    -   [ ] Commit: `Create Notification model struct.`
    -   [ ] Commit: `Create Subscriber database and Subscriber repository struct skeleton.`
    -   [ ] Commit: `Implement add function in Subscriber repository.`
    -   [ ] Commit: `Implement list_all function in Subscriber repository.`
    -   [ ] Commit: `Implement delete function in Subscriber repository.`
    -   [ ] Write answers of your learning module's "Reflection Publisher-1" questions in this README.
-   **STAGE 2: Implement services and controllers**
    -   [ ] Commit: `Create Notification service struct skeleton.`
    -   [ ] Commit: `Implement subscribe function in Notification service.`
    -   [ ] Commit: `Implement subscribe function in Notification controller.`
    -   [ ] Commit: `Implement unsubscribe function in Notification service.`
    -   [ ] Commit: `Implement unsubscribe function in Notification controller.`
    -   [ ] Write answers of your learning module's "Reflection Publisher-2" questions in this README.
-   **STAGE 3: Implement notification mechanism**
    -   [ ] Commit: `Implement update method in Subscriber model to send notification HTTP requests.`
    -   [ ] Commit: `Implement notify function in Notification service to notify each Subscriber.`
    -   [ ] Commit: `Implement publish function in Program service and Program controller.`
    -   [ ] Commit: `Edit Product service methods to call notify after create/delete.`
    -   [ ] Write answers of your learning module's "Reflection Publisher-3" questions in this README.

## Your Reflections
This is the place for you to write reflections:

### Mandatory (Publisher) Reflections

#### Reflection Publisher-1
1. Dalam kasus ini, penggunaan sebuah interface (atau trait dalam Rust) untuk Subscriber tidak sepenuhnya diperlukan karena tidak ada kebutuhan untuk berbagai implementasi dari subscriber yang berbeda perilaku. Sebuah Model struct tunggal sudah cukup untuk mewakili subscriber karena tidak ada indikasi bahwa subscriber akan memiliki perilaku yang berbeda-beda. Dalam pola Observer, kebutuhan akan interface atau trait muncul ketika ada variasi perilaku antara subjek yang diamati dan para pelanggan (subscriber). Jika tidak ada variasi tersebut, menggunakan struct tunggal sudah cukup.
   
2. Dalam kasus ini, penggunaan Vec (list) tidak cukup karena mencari nilai yang unik akan memakan waktu linear. DashMap (map/dictionary) yang digunakan dalam implementasi BambangShop memberikan kinerja yang lebih baik untuk operasi pencarian dan manipulasi data yang unik. Oleh karena itu, penggunaan DashMap lebih tepat untuk memastikan keunikan id dan url.

3. Dalam Rust, `thread-safe` sangat penting dan kompilator Rust memberikan penekanan yang kuat pada hal ini. Dalam kasus variabel statis SUBSCRIBERS, penggunaan DashMap untuk HashMap yang aman untuk benang adalah langkah yang tepat. Pola Singleton sendiri tidak secara otomatis memberikan `thread-safe`, tetapi hanya memastikan bahwa hanya ada satu instansi dari sebuah objek. Dengan menggunakan DashMap, kita mendapatkan keuntungan dari `thread-safe` yang disediakan oleh implementasinya, sehingga memastikan bahwa SUBSCRIBERS dapat diakses secara aman oleh beberapa `thread` secara bersamaan. Sehingga, dalam konteks ini, DashMap lebih sesuai daripada Pola Singleton.

#### Reflection Publisher-2
1. Pemisahan `Service` dan `Repoitory` dar model di dasarkan dari prinsip-prinsip desain yang mengedepankan pemisahan tanggung jawab dan abstraksi yang baik pada saat pengembangan . Dengan pemisahan `Service` dan `Repository` dapat memungkinkan fleksibilitas dalam pengembangan, memungkinkan penggantian teknologi penyimpanan data tanpa mengganggu `business logic`, dan meningkatkan kejelasan dan pemeliharaan kode secara keseluruhan.

2. Jika hanya menggunakan Model, kompleksitas kode untuk setiap model (Program, Subscriber, Notification) akan meningkat karena setiap model harus menangani baik `business logic` maupun `data storage`. Hal ini akan mengakibatkan keterikatan yang erat antar model, potensi duplikasi kode, serta kesulitan dalam pemeliharaan dan pengujian. Misalnya, Model Program harus berinteraksi langsung dengan Model Subscriber dan Model Notification untuk mengelola langganan dan pengiriman notifikasi. Hal ini akan meningkatkan kompleksitas dan ketergantungan antar model, serta membuat kode sulit untuk dimengerti dan dikembangkan secara terpisah sehingga dengan memisahkan tanggung jawab menjadi lapisan Service dan Repository, kita dapat meningkatkan kejelasan dan pemeliharaan kode, serta memudahkan pengujian dan pengembangan secara terpisah untuk setiap model.

3. Sebelum saya telah mengetahui postman pada saat pengembangan proyek mata kuliah PBP sehingga berikut adalah list bagaimana postman sangat membantu saya :

- **Membangun Permintaan API**: Postman menyediakan antarmuka pengguna yang intuitif untuk membangun permintaan API dengan berbagai metode, parameter, dan muatan yang berbeda.

- **Validasi Respons API**: Postman memungkinkan untuk memvalidasi respons API untuk memastikan bahwa mereka sesuai dengan format yang diharapkan dan berisi data yang benar.

- **Otomatisasi Pengujian**: Postman mendukung otomatisasi pengujian API, memungkinkan untuk membuat skrip pengujian dan menjalankannya secara otomatis.

- **Koleksi Permintaan**: Postman memungkinkan untuk menyimpan dan mengelola koleksi permintaan API, yang memudahkan pengorganisasian dan pengelolaan setiap permintaan yang telah dibuat.

- **Dokumentasi API**: Postman juga menyediakan fitur untuk membuat dokumentasi API yang terstruktur dan mudah dipahami, membantu dalam berbagi informasi tentang API dengan anggota tim atau pengguna lainnya.

Dalam proyek kelompok atau proyek rekayasa perangkat lunak masa depan, Postman akan terbukti sangat berguna dengan fitur-fitur ini, yang dapat membantu meningkatkan kualitas  aplikasi, serta mempercepat siklus pengembangan secara keseluruhan.

#### Reflection Publisher-3
 
1. Pada tutorial ini menggunakan varian model `Push model (publisher pushes data to subscribers)` . Hal ini terlihat dari implementasi `NotificationService` secara aktif nge-push notifikasi ke setiap subscriber dengan menggunakan metode `update`.
2. Berikut adalah kelebihan dan kekurangan menggunakan varian `Pull model (subscribers pull data from publisher)`.

Kelebihan :
- Subscriber memiliki lebih banyak kontrol kapan mereka menerima pembaruan. Mereka dapat menarik data dari publisher ketika mereka siap untuk memprosesnya, yang dapat mengurangi risiko `spam` subscriber dengan notifikasi.
- Model Pull dapat lebih efisien ketika subscriber selektif tentang data yang mereka butuhkan, karena mereka hanya menarik data yang mereka perlukan.
  
kekurangan :
- Kompleksitas implementasi meningkat karena subscriber perlu secara aktif menarik data dari publisher, yang mungkin memerlukan mekanisme komunikasi yang lebih canggih.
  
- Ada kemungkinan keterlambatan antara ketersediaan pembaruan dan subscriber menariknya, yang dapat menyebabkan penundaan dalam menerima notifikasi.

1. Jika kita memutuskan untuk tidak menggunakan `multi-threading` dalam proses notifikasi, program akan memproses notifikasi untuk setiap subscriber secara berurutan dalam thread utama. Hal ini berarti pengiriman notifikasi ke setiap subscriber akan bersifat blocking, menyebabkan program menunggu setiap proses notifikasi selesai sebelum beralih ke subscriber berikutnya. Akibatnya, proses notifikasi keseluruhan mungkin memakan waktu lebih lama untuk selesai, berpotensi menyebabkan penundaan dalam menangani tugas lain dan mengurangi responsivitas aplikasi. 